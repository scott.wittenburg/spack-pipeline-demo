# spack-pipeline-demo

This repository contains a simple example of a spack gitlab pipeline that requires no special runners, publicly accessible mirrors, or other infrastructure.  The repo consists only of a `gitlab-ci.yml` to configure the pipeline, and a simple spack environment (in the form of a `spack.yaml` file).

You can read spack's official pipeline documentation [here](https://spack.readthedocs.io/en/latest/pipelines.html).

